require("dotenv").config();
const express = require("express");
const morgan = require("morgan");

const app = express();
const {
    notFoundHandler,
    errorHandler,
} = require("./src/controllers/ErrorController");
const homeRoute = require("./src/routes/homeRoute");
const userRoute = require("./src/routes/userRoute");

app.set("view engine", "ejs");
app.use(morgan("dev"));
app.use(express.static("public"));
app.use(express.json());

// homepage
app.use("/", homeRoute);

// users api
app.use("/api/v1/users", userRoute);

// not found handler
app.use(notFoundHandler);

// error handler
app.use(errorHandler);

app.listen(process.env.PORT, () => {
    console.log(`Running on ${process.env.PORT}`);
});
