// player
class Player {
    constructor(){
        if(this.constructor === Player){
            throw new Error('This is an abstraction class !')
        };
        this.score = 0;
        this.win = null;
        this.selected = null;
    }

    reset(){
        this.win = null;
        this.selected = null;
    }

    resetScore(){
        this.score = 0;
    }
}

class UserPlayer extends Player{
    constructor(){
        super();
    }

    choose(choice){
        this.selected = choice;
    }
}

class ComPlayer extends Player{
    constructor(){
        super()
    }

    roll(btnCom){
        const choices = Object.values(RockPaperScissor.choices);
        const randIdx = Math.floor(Math.random() * choices.length);
        this.selected = choices[randIdx];
        btnCom.forEach(function (el){
            el.classList.remove('active');
            if(el.dataset.choice === choices[randIdx]){
                el.classList.add('active');
            }
        })
    }
}

// Game
class Game{
    constructor(players){
        if(this.constructor === Game){
            throw new Error("This is an abstraction class !");
        };
        this.players = players;
    }

    reset() {
        for(const player of players){
            player.reset();
        }
    }
}

class RockPaperScissor extends Game{
    static choices = {
        ROCK: 'rock',
        PAPER: 'paper',
        SCISSOR: 'scissor'
    }

    constructor(userPlayer, comPlayer){
        super([
            userPlayer,
            comPlayer
        ])
    }

    userTurn(choice){
        this.players[0].choose(choice);
    }

    comTurn(btnCom, score, result){
        const interval = setInterval(() => {
            this.players[1].roll(btnCom);
        }, 100);

        setTimeout(() => {
            clearInterval(interval)

            // result
            this.#result(score, result);
        }, 2000);
    }

    #result(score, result){
        const [user, comp] = this.players;
        this.#detemineResult(user, comp);
        score.userScore.innerText = user.score;
        score.comScore.innerText = comp.score;
        score.btnReset.removeAttribute('disabled');

        result.versus.style.display = 'none';
        if(user.win){
            result.userWin.style.display = 'block';
        }else if(comp.win){
            result.comWin.style.display = 'block';
        }else if(!comp.win && !user.win){
            result.draw.style.display = 'block';
        }
    }

    #detemineResult(user, comp){
        if(user.selected === RockPaperScissor.choices.ROCK){
            if(comp.selected === RockPaperScissor.choices.PAPER){
                comp.win = true;
                user.win = false;
                comp.score++;
            }else if(comp.selected === RockPaperScissor.choices.SCISSOR){
                user.win = true;
                comp.win = false;
                user.score++;
            }else{
                user.win = false;
                comp.win = false;
            }
        }else if(user.selected === RockPaperScissor.choices.PAPER){
            if(comp.selected === RockPaperScissor.choices.SCISSOR){
                comp.win = true;
                user.win = false;
                comp.score++;
            }else if(comp.selected === RockPaperScissor.choices.ROCK){
                user.win = true;
                comp.win = false;
                user.score++;
            }else{
                user.win = false;
                comp.win = false;
            }
        }else if(user.selected === RockPaperScissor.choices.SCISSOR){
            if(comp.selected === RockPaperScissor.choices.ROCK){
                comp.win = true;
                user.win = false;
                comp.score++;
            }else if(comp.selected === RockPaperScissor.choices.PAPER){
                user.win = true;
                comp.win = false;
                user.score++;
            }else{
                user.win = false;
                comp.win = false;
            }
        }
    }
}