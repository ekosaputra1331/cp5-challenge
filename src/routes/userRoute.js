const express = require("express");
const UserController = require("../controllers/UserController");
const authenticate = require("./../middleware/authenticate");
const router = express.Router();

router.post("/register", UserController.register);
router.post("/login", UserController.login);
router.get("/me", authenticate, UserController.me);
router.put("/me", authenticate, UserController.updateMe);
router.delete("/me", authenticate, UserController.deleteMe);
router.get("/getusers", authenticate, UserController.getUsers);
router.get("/getuser/:id", authenticate, UserController.getUser);
router.delete("/getuser/:id", authenticate, UserController.deleteUser);

module.exports = router;