function responHandler(res, success, data, message, code = 200) {
    res.status(code).json({
        success,
        data,
        message,
        code
    });
}

function errorWrapper(res, success, message, error, code){
    res.status(code).json({
        success,
        message,
        error,
        code
    })
}

module.exports = {
    responHandler,
    errorWrapper
};
