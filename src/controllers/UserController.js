const path = require("path");
const fs = require("fs").promises;
const bcrypt = require("bcrypt");
const { v4: uuidv4 } = require("uuid");
const util = require("util");
const jwt = require("jsonwebtoken");

const jwtSignAsync = util.promisify(jwt.sign);
const { responHandler } = require("../utils/responseHandler");
const { ValidateError } = require("./ErrorController");
const fullPath = path.resolve(__dirname, "../data/users.json");

async function register(req, res, next) {
    const {
        username,
        email,
        password,
        age = null,
        address = null,
        isAdmin = false,
    } = req.body;

    try {
        if (!username || !email || !password) {
            throw new ValidateError("Missing Credentials");
        }
        // getting all users
        let users = await fs.readFile(fullPath, "utf-8");
        users = JSON.parse(users);

        // checking if the user already exits
        if (users.some((x) => x.username === username || x.email === email)) {
            throw new ValidateError("User already exists");
        }

        // create new user
        const newUser = {
            id: uuidv4(),
            username,
            password: await getHashPassword(password),
            email,
            age,
            address,
            isAdmin,
        };

        users.push(newUser);

        await fs.writeFile(fullPath, JSON.stringify(users), "utf-8");

        // creating token for authentication
        return responHandler(
            res,
            true,
            {
                username,
                token: await generateToken(newUser.id),
            },
            "User created",
            201
        );
    } catch (error) {
        next(error);
    }
}

async function login(req, res, next) {
    const { username, password } = req.body;

    try {
        if (!username || !password) {
            throw new ValidateError("Missing Credentials");
        }
        // getting all users
        let users = await fs.readFile(fullPath, "utf-8");

        users = JSON.parse(users);

        // checking user
        const foundUser = users.find(
            (x) => x.username === username || x.email === username
        );

        if (
            !foundUser ||
            !(await bcrypt.compare(password, foundUser.password))
        ) {
            throw new ValidateError("Invalid credentials");
        }

        // creating token
        return responHandler(
            res,
            true,
            {
                username: foundUser.username,
                token: await generateToken(foundUser.id),
            },
            "Login successfull",
            200
        );
    } catch (error) {
        next(error);
    }
}

function me(req, res) {
    return responHandler(res, true, req.user, "My info");
}

async function getUsers(req, res, next) {
    try {
        // getting all users
        let users = await fs.readFile(fullPath, "utf-8");
        users = JSON.parse(users);

        // remove all password
        users.map((x) => delete x.password);

        return responHandler(res, true, users, "Get users", 200);
    } catch (error) {
        next(error);
    }
}

async function updateMe(req, res, next) {
    const { age, address } = req.body;

    try {
        let users = await fs.readFile(fullPath, "utf-8");
        users = JSON.parse(users);

        // getting user
        const userIndex = users.findIndex((x) => x.id === req.user.id);
        if (userIndex < 0) {
            throw new ValidateError("User not found");
        }

        // updating user
        users[userIndex].age = age;
        users[userIndex].address = address;

        // save all users
        await fs.writeFile(fullPath, JSON.stringify(users), "utf-8");

        delete users[userIndex].password;

        return responHandler(
            res,
            true,
            users[userIndex],
            "User updated successfully",
            200
        );
    } catch (error) {
        next(error);
    }
}

async function deleteMe(req, res, next) {
    try {
        let users = await fs.readFile(fullPath, "utf-8");
        users = JSON.parse(users);

        // getting user
        const userIndex = users.findIndex((x) => x.id === req.user.id);
        if (userIndex < 0) {
            throw new ValidateError("User not found");
        }

        // delete users
        const deletedUser = users.splice(userIndex, 1);

        // save remains users
        await fs.writeFile(fullPath, JSON.stringify(users), "utf-8");

        return responHandler(
            res,
            true,
            deletedUser,
            "User deleted successfully"
        );
    } catch (error) {
        next(error);
    }
}

async function getUser(req, res, next) { 
    try {
        // getting all users
        let users = await fs.readFile(fullPath, "utf-8");
        users = JSON.parse(users);

        // remove all password
        const foundUser = users.find((x) => x.id === req.params.id);
        if (!foundUser) {
            return responHandler(res, false, null, "User not found", 404);
        }

        delete foundUser.password;

        return responHandler(res, true, foundUser, "Get user", 200);
    } catch (error) {
        next(error);
    }
}

async function deleteUser(req, res, next) {
    try {
        if(req.user.isAdmin){
            throw new ValidateError("Can not delete admin", 403);
        }

        let users = await fs.readFile(fullPath, "utf-8");
        users = JSON.parse(users);

        // getting user
        const userIndex = users.findIndex((x) => x.id === req.params.id);
        if (userIndex < 0) {
            throw new ValidateError("User not found");
        }

        // delete users
        const deletedUser = users.splice(userIndex, 1);

        // save remains users
        await fs.writeFile(fullPath, JSON.stringify(users), "utf-8");

        return responHandler(
            res,
            true,
            deletedUser,
            "User deleted successfully"
        );
    } catch (error) {
        next(error);
    }
}

async function getHashPassword(password) {
    const salt = await bcrypt.genSalt(parseInt(process.env.SALT));
    return await bcrypt.hash(password, salt);
}

async function generateToken(id) {
    return await jwtSignAsync({ id }, process.env.SECRET_TOKEN, {
        expiresIn: "1h",
    });
}

module.exports = {
    register,
    login,
    me,
    updateMe,
    deleteMe,
    getUsers,
    getUser,
    deleteUser,
};
