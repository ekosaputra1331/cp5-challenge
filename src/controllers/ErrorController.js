const { TokenExpiredError } = require("jsonwebtoken");
const { responHandler, errorWrapper } = require("../utils/responseHandler");

class AuthenticateError extends Error {
    constructor(message, statusCode = 401) {
        super(message);
        this.statusCode = statusCode;
        this.name = this.constructor.name;
    }
}

class ValidateError extends Error {
    constructor(message, statusCode = 400) {
        super(message);
        this.statusCode = statusCode;
        this.name = this.constructor.name;
    }
}

// default handler for not found
function notFoundHandler(req, res, next) {
    const acceptType = req.headers.accept.split(",")[0];
    if (acceptType === "application/json") {
        return responHandler(res, false, null, "Not Found", 404);
    }

    return res.status(404).render("404page", {
        page: '404',
        title: "404",
    });
}

// error handler
function errorHandler(err, req, res, next) {
    if (err instanceof AuthenticateError) {
        return errorWrapper(res, false, err.message, err.name, err.statusCode);
    }

    if (err instanceof ValidateError) {
        return errorWrapper(res, false, err.message, err.name, err.statusCode);
    }

    if (err instanceof TokenExpiredError) {
        return errorWrapper(res, false, err.message, err.name, 401);
    }

    return errorWrapper(res, false, err.message, err.name, 500);
}

module.exports = {
    notFoundHandler,
    errorHandler,
    AuthenticateError,
    ValidateError,
};
