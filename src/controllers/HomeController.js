function home(req,res){
    res.status(200).render("index", {
        page: 'home',
        title: 'Traditional Game',
    });
}

function game(req, res) {
    res.status(200).render("games", {
        page: 'game',
        title: 'Rock Paper Scissor'
    });
}

module.exports = {
    game,
    home
}