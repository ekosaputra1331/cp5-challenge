const util = require("util");
const jwt = require("jsonwebtoken");
const fs = require("fs").promises;
const path = require("path");

const jwtVerifyAsync = util.promisify(jwt.verify);
const { AuthenticateError } = require("../controllers/ErrorController");

const fullPath = path.resolve(__dirname, "../data/users.json");

async function authenticate(req, res, next) {
    // getting token
    const header = req.headers.authorization;
    const token = header && header.split(" ")[1];
    
    try {
        if (!token) {
            throw new AuthenticateError("No Token");
        }
        // verifying token
        const { id } = await jwtVerifyAsync(token, process.env.SECRET_TOKEN);

        // getting user by id
        let users = await fs.readFile(fullPath, "utf-8");
        users = JSON.parse(users);

        const foundUser = users.find((x) => (x.id === id));
        if (!foundUser) {
            throw new AuthenticateError("No Authenticated User");
        }
        
        delete foundUser.password;

        req.user = foundUser;
        next();
    } catch (error) {
        next(error);
    }
}

module.exports = authenticate;
